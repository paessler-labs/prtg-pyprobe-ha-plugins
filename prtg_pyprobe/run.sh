#!/usr/bin/env bashio
set -v

CONFIG_PATH=/data/options.json
READ_PROBE_CONFIG=$(jq --raw-output '' $CONFIG_PATH)
export PROBE_CONFIG=$READ_PROBE_CONFIG
cd /pyprobe-venv/lib/python3.8/site-packages/prtg_pyprobe/
exec "$@"
